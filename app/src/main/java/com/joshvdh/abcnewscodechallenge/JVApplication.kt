package com.joshvdh.abcnewscodechallenge

import android.app.Application
import com.joshvdh.abcnewscodechallenge.data.DatabaseHelper
import com.joshvdh.abcnewscodechallenge.network.NetworkHelper
import com.joshvdh.abcnewscodechallenge.utils.DimenHelper
import com.joshvdh.abcnewscodechallenge.utils.JVColors
import com.joshvdh.abcnewscodechallenge.utils.JVFonts

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class JVApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        JVColors.init(this)
        JVFonts.init(this)
        DimenHelper.init(this)
        DatabaseHelper.init(this)
        NetworkHelper.init(this)
    }
}
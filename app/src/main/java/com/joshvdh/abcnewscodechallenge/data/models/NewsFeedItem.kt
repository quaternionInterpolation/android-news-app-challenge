package com.joshvdh.abcnewscodechallenge.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.annotation.NonNull
import com.joshvdh.abcnewscodechallenge.utils.JVDateFormat
import com.joshvdh.abcnewscodechallenge.utils.optDate
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
@Entity
class NewsFeedItem {
    @NonNull
    @PrimaryKey lateinit var id: String
    @ColumnInfo(name = "title") var title: String? = null
    @ColumnInfo(name = "pubdate") var pubdate: Date? = null
    @ColumnInfo(name = "link") var link: String? = null
    @ColumnInfo(name = "author") var author: String? = null
    @ColumnInfo(name = "thumbnailURL") var thumbnailURL: String? = null
    @ColumnInfo(name = "imageURL") var imageURL: String? = null
    @ColumnInfo(name = "description") var description: String? = null
    @ColumnInfo(name = "lastParsed") var lastParsed: Date? = null

    companion object {
        /**
         * JSON example:
         {
            "title": "WA Premier defends MP over alleged Chinese Communist Party affiliation",
            "pubDate": "2018-12-04 10:26:58",
            "link": "http://www.abc.net.au/news/2018-12-04/wa-premier-defends-mp-over-alleged-chinese-communist-party-links/10583028",
            "guid": "http://www.abc.net.au/news/2018-12-04/wa-premier-defends-mp-over-alleged-chinese-communist-party-links/10583028",
            "author": "Eliza Borrello",
            "thumbnail": "http://www.abc.net.au/news/image/10582532-4x3-140x105.jpg",
            "description": "<p>Labor's Upper House whip Pierre Yang says he \"overlooked\" declaring his membership of two organisations allegedly affiliated with China's Communist Party on his parliamentary register of interests, as Premier Mark McGowan says he has full confidence in the Chinese-born MP.</p>",
            "content": "<p>Labor's Upper House whip Pierre Yang says he \"overlooked\" declaring his membership of two organisations allegedly affiliated with China's Communist Party on his parliamentary register of interests, as Premier Mark McGowan says he has full confidence in the Chinese-born MP.</p>",
            "enclosure": {
                "link": "http://www.abc.net.au/news/image/10582532-16x9-2150x1210.jpg",
                "type": "image/jpeg",
                "thumbnail": "http://www.abc.net.au/news/image/10582532-4x3-140x105.jpg"
            },
            "categories": [
                "State Parliament",
                "States and Territories",
                "Government and Politics"
            ]
        }
         */
        private val KEY_TITLE = "title"
        private val KEY_PUBDATE = "pubDate"
        private val KEY_LINK = "link"
        private val KEY_GUID = "guid"
        private val KEY_AUTHOR = "author"
        private val KEY_THUMBNAIL_URL = "thumbnail"

        //Enclosure
        private val KEY_ENCLOSURE = "enclosure"
        private val KEY_ENCLOSURE_IMAGE_URL = "link"
        private val KEY_DESCRIPTION = "description"

        private val dateFormatter = JVDateFormat.of("yyyy-MM-dd HH:mm:ss")
    }


    fun fromJSON(json: JSONObject): NewsFeedItem {
        id = json.optString(KEY_GUID)
        title = json.optString(KEY_TITLE, title)
        link = json.optString(KEY_LINK, link)
        pubdate = json.optDate(KEY_PUBDATE, pubdate, dateFormatter)
        author = json.optString(KEY_AUTHOR, author)
        thumbnailURL = json.optString(KEY_THUMBNAIL_URL)

        json.optJSONObject(KEY_ENCLOSURE)?.let {
            imageURL = it.optString(KEY_ENCLOSURE_IMAGE_URL)
        }

        description = json.optString(KEY_DESCRIPTION)

        lastParsed = Date()

        return this
    }
}
package com.joshvdh.abcnewscodechallenge.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItemDao

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
@Database(entities = [NewsFeedItem::class],
    version = 1)
@TypeConverters(DataTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsFeedItemDao(): NewsFeedItemDao
}
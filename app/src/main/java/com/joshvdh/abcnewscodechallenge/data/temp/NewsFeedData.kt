package com.joshvdh.abcnewscodechallenge.data.temp

import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.network.IJsonSerializable
import org.json.JSONObject

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
class NewsFeedData: IJsonSerializable {
    var status: Status? = null
    val parsedItems: MutableList<NewsFeedItem> = mutableListOf()

    enum class Status(val statusString: String?) {
        OK("ok"),
        UNKNOWN(null);

        companion object {
            fun fromString(status: String?): Status = try {
                values().find { it.statusString == status } ?: UNKNOWN
            } catch (e: Exception) {
                UNKNOWN
            }
        }
    }

    companion object {
        private val KEY_STATUS = "status"
        private val KEY_ITEMS = "items"
    }

    override fun fromJson(json: JSONObject): NewsFeedData {
        status = Status.fromString(json.optString(KEY_STATUS))
        parsedItems.clear()

        val itemArray = json.optJSONArray(KEY_ITEMS)
        for (i in 0 until itemArray.length()) {
            itemArray.optJSONObject(i)?.let {
                parsedItems.add(NewsFeedItem()
                    .fromJSON(it))
            }
        }
        return this
    }
}
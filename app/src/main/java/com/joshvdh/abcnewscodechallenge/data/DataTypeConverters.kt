package com.joshvdh.abcnewscodechallenge.data

import androidx.room.TypeConverter
import java.util.*

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
class DataTypeConverters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}
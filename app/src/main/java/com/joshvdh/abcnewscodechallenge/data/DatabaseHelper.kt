package com.joshvdh.abcnewscodechallenge.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.utils.JLog
import com.joshvdh.abcnewscodechallenge.utils.TaskManager
import java.util.*

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
object DatabaseHelper {
    private val DATABASE_NAME = "NewsCodeChallengeDatabase"

    private lateinit var database: AppDatabase

    lateinit var allNewsFeedItems: LiveData<List<NewsFeedItem>>

    fun init(appContext: Context) {

        database = Room.databaseBuilder(appContext, AppDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .build()

        allNewsFeedItems = database.newsFeedItemDao().getAll()

        allNewsFeedItems.observeForever {
            JLog.e("All news updated")
        }
    }

    fun addNewsFeedItems(vararg items: NewsFeedItem) {
        TaskManager.postToWorker {
            database.runInTransaction {
                var parseDate = Date()
                database.newsFeedItemDao().deleteAllBefore(parseDate)
                database.newsFeedItemDao().insertAll(*items)
            }
        }
    }

    fun getNewsFeedItems(vararg ids: Int): LiveData<List<NewsFeedItem>> {
        return database.newsFeedItemDao().getAllByIds(ids)
    }
}
package com.joshvdh.abcnewscodechallenge.data.models

import androidx.lifecycle.LiveData
import androidx.room.*
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import java.util.*

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
@Dao
interface NewsFeedItemDao {
    @Query("SELECT * FROM newsfeeditem")
    fun getAll(): LiveData<List<NewsFeedItem>>

    @Query("SELECT * FROM newsfeeditem WHERE id IN (:feedIds)")
    fun getAllByIds(feedIds: IntArray): LiveData<List<NewsFeedItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg items: NewsFeedItem)

    @Delete
    fun delete(item: NewsFeedItem)

    @Query("DELETE FROM newsfeeditem WHERE lastParsed < :date")
    fun deleteAllBefore(date: Date)
}
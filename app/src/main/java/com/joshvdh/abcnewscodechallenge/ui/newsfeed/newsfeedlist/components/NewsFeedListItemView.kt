package com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist.components

import android.content.Context
import android.net.Uri
import android.text.Html
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.TextAppearanceSpan
import android.text.style.TypefaceSpan
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.RelativeLayout
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.ui.components.JVTextView
import com.joshvdh.abcnewscodechallenge.utils.JVColors
import com.joshvdh.abcnewscodechallenge.utils.JVDateFormat
import com.joshvdh.abcnewscodechallenge.utils.LayoutBuilder
import com.joshvdh.abcnewscodechallenge.utils.loadImageAsync

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class NewsFeedListItemView: RelativeLayout {

    var background: RelativeLayout? = null
    var image: ImageView? = null
    var titleText: JVTextView? = null
    var dateText: JVTextView? = null

    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1)
            : super(context, attrs, defStyleAttr) {

        background = RelativeLayout(context).apply {
            setBackgroundColor(JVColors.BACKGROUND_LIGHT.value)
        }
        addView(background, LayoutBuilder
            .rel(context)
            .matchWrap()
            .margin(20)
            .build())

        image = ImageView(context).apply {
            scaleType = ImageView.ScaleType.CENTER_CROP
        }
        background?.addView(image, LayoutBuilder
            .rel(context)
            .of(150)
            .alignEnd()
            .build())

        titleText = JVTextView(context).apply {
            textSizeDP = 18f
            setTextColor(JVColors.NEWS_HEADER.value)
        }
        background?.addView(titleText, LayoutBuilder
            .rel(context)
            .wrap()
            .margin(20f)
            .startOf(image!!)
            .build())

        dateText = JVTextView(context).apply {
            textSizeDP = 12f
            setTextColor(JVColors.NEWS_DATE.value)
        }
        background?.addView(dateText, LayoutBuilder
            .rel(context)
            .wrap()
            .marginX(20f)
            .marginBottom(20f)
            .below(titleText!!)
            .build())
    }

    val dateFormatter = JVDateFormat.of("MMM d, yyyy")
    val timeFormatter = JVDateFormat.of("hh:mm a")
    fun bind(data: NewsFeedItem?) {
        data?.let {
            titleText?.text = Html.fromHtml(it.title)

            data.thumbnailURL?.let { url ->
                image?.loadImageAsync(url)
            }

            //TODO: Set spannable to bolden date before time
            val dateString = dateFormatter.format(data.pubdate)
            val timeString = timeFormatter.format(data.pubdate)
            val final = String.format("<strong>%s</strong> %s", dateString, timeString)
            dateText?.text = Html.fromHtml(final)
        }
    }
}
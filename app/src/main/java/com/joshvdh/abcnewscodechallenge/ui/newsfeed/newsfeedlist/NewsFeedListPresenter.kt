package com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import android.util.Log
import com.joshvdh.abcnewscodechallenge.data.DatabaseHelper
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.mvp.BasePresenter
import com.joshvdh.abcnewscodechallenge.network.NetworkHelper
import com.joshvdh.abcnewscodechallenge.utils.JLog

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class NewsFeedListPresenter(view: NewsFeedListMvpView): BasePresenter<NewsFeedListMvpView>(view), NewsFeedListMvpPresenter,
    Observer<List<NewsFeedItem>> {

    override fun onResume() {
        super.onResume()

        DatabaseHelper
            .allNewsFeedItems
            .observe(view.getLifecycleOwner(), this)

        refreshData()
    }

    override fun refreshData() {
        view.onLoadingChanged(true)

        NetworkHelper.fetchFeed(object: NetworkHelper.NetworkCallback{
            override fun onResult(success: Boolean, code: Int?, message: String?) {
                view.onLoadingChanged(false)
            }
        })
    }

    override fun onChanged(data: List<NewsFeedItem>?) {
        //Update view with new data!
        data?.let {
            view.onDataUpdated(it)
        }
    }
}
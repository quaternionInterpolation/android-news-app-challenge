package com.joshvdh.abcnewscodechallenge.ui.splash

import com.joshvdh.abcnewscodechallenge.BuildConfig
import com.joshvdh.abcnewscodechallenge.mvp.BasePresenter
import com.joshvdh.abcnewscodechallenge.utils.TaskManager

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class SplashPresenter(view: SplashMvpView): BasePresenter<SplashMvpView>(view), SplashMvpPresenter {
    override fun onResume() {
        super.onResume()
        // Start a fake countdown to display a logo and proceed to main activity
        TaskManager.postDelayed(
            BuildConfig.SPLASH_DURATION_MS,
            showNewsFeedRunnable)
    }

    override fun onPause() {
        super.onPause()
        TaskManager.cancelPost(showNewsFeedRunnable)
    }

    val showNewsFeedRunnable = {
        view.showNewsFeed()
    }
}
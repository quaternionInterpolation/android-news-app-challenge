package com.joshvdh.abcnewscodechallenge.ui

import android.content.Intent
import android.os.Bundle
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import com.joshvdh.abcnewscodechallenge.mvp.MvpPresenter
import com.joshvdh.abcnewscodechallenge.mvp.MvpView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
abstract class BaseActivity<T: MvpPresenter> : MvpView, AppCompatActivity() {
    protected abstract val presenter: T?

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    fun presentActivity(clazz: Class<*>, bundle: Bundle? = null) {
        val intent = Intent(this, clazz)
        if (bundle != null) startActivity(intent, bundle) else startActivity(intent)
    }

    //As noted in BaseFragment:
    //Note: This is recognisably not good, hack to keep MVP architecture in place.
    // Ideally if more time permitted, the app would use MVVM
    override fun getLifecycleOwner(): LifecycleOwner = this
}
package com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist

import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.mvp.MvpPresenter
import com.joshvdh.abcnewscodechallenge.mvp.MvpView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
interface NewsFeedListMvpView: MvpView {
    fun onDataUpdated(items: List<NewsFeedItem>)
    fun onLoadingChanged(isLoading: Boolean)
}

interface NewsFeedListMvpPresenter: MvpPresenter {
    fun refreshData()
}
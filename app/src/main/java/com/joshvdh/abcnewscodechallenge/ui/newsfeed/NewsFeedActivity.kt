package com.joshvdh.abcnewscodechallenge.ui.newsfeed

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.joshvdh.abcnewscodechallenge.R
import com.joshvdh.abcnewscodechallenge.ui.BaseActivity
import com.joshvdh.abcnewscodechallenge.ui.components.MenuBar
import com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist.NewsFeedListFragment
import com.joshvdh.abcnewscodechallenge.utils.bindView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class NewsFeedActivity: BaseActivity<NewsFeedPresenter>(), NewsFeedMvpView {
    override val presenter by lazy { NewsFeedPresenter(this) }

    val menuBar by bindView<MenuBar>(R.id.newsfeed_menubar)

    @IdRes
    val fragmentHostId: Int = R.id.newsfeed_fragment_container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_newsfeed)

        presenter.initialize()
    }

    override fun showNewsFeedList() {
        val fragment = NewsFeedListFragment()
        switchContentFragment(fragment)
    }

    fun switchContentFragment(fragment: Fragment, animate: Boolean = false) {
        supportFragmentManager.beginTransaction().apply {
            replace(fragmentHostId, fragment)

            if (animate) {
                setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out)
            }

            commit()
        }
    }
}
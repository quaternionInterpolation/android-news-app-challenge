package com.joshvdh.abcnewscodechallenge.ui.splash

import com.joshvdh.abcnewscodechallenge.mvp.MvpPresenter
import com.joshvdh.abcnewscodechallenge.mvp.MvpView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
interface SplashMvpView: MvpView {
    fun showNewsFeed()
}

interface SplashMvpPresenter: MvpPresenter {
    //Stub
}
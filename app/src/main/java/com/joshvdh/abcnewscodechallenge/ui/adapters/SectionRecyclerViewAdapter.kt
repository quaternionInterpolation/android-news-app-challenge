package com.joshvdh.abcnewscodechallenge.ui.adapters

import androidx.recyclerview.widget.RecyclerView

/**
 *  Created by Josh van den Heever on 6/12/18.
 */
abstract class SectionRecyclerViewAdapter<T: RecyclerView.ViewHolder>: RecyclerView.Adapter<T>() {

    data class SectionIndex (
        val section: Int,
        val position: Int)

    override fun getItemCount(): Int {
        var result = 0
        for (i in 0 until getSectionCount()) {
            result += getItemCount(i)
        }
        return result
    }

    override fun onBindViewHolder(viewHolder: T, position: Int) {
        val index = getIndex(position)
        onBindViewHolder(viewHolder, index.section, index.position, position)
    }

    private fun getIndex(position: Int): SectionIndex {
        var result = position
        for (i in 0 until getSectionCount()) {
            val sectionSize = getItemCount(i)
            if (result < sectionSize) {
                return SectionIndex(i, result)
            }

            result -= sectionSize
        }
        return SectionIndex(0, result)
    }

    override fun getItemId(position: Int): Long {
        val index = getIndex(position)
        return getItemId(index.section, index.position, position)
    }

    override fun getItemViewType(position: Int): Int {
        val index = getIndex(position)
        return getItemViewType(index.section, index.position, position)
    }

    abstract fun getItemCount(section: Int): Int
    abstract fun getSectionCount(): Int
    abstract fun onBindViewHolder(viewHolder: T, section: Int, sectionPosition: Int, adapterPosition: Int)
    abstract fun getItemId(section: Int, sectionPosition: Int, adapterPosition: Int): Long
    abstract fun getItemViewType(section: Int, position: Int, adapterPosition: Int): Int
}
package com.joshvdh.abcnewscodechallenge.ui.components

import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class JVViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    inline fun <reified T: View> getView() : T? {
        return itemView as? T
    }
}
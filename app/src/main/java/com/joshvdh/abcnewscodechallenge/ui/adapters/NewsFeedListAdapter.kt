package com.joshvdh.abcnewscodechallenge.ui.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.ui.components.JVViewHolder
import com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist.components.NewsFeedListFeaturedItemView
import com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist.components.NewsFeedListItemView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class NewsFeedListAdapter: SectionRecyclerViewAdapter<JVViewHolder>() {

    enum class CellType {
        NewsCellFeatured,
        NewsCellRegular
    }

    val items: MutableList<NewsFeedItem> = mutableListOf()

    fun setData(items: List<NewsFeedItem>) {
        this.items.clear()
        this.items.addAll(items)

        notifyDataSetChanged()

        //TODO: Use DiffUtil to determine changed items and notify items of change
    }

    override fun getItemCount(section: Int): Int = when(CellType.values()[section]) {
        CellType.NewsCellFeatured -> 1
        CellType.NewsCellRegular -> items.size-1
    }

    override fun getSectionCount(): Int {
        var result = 0
        if (items.size > 0) result += 1 //Feature
        if (items.size > 1) result += 1 //Regular
        return result
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): JVViewHolder {
        val cellType = CellType.values()[type]
        val view: View

        view = when(cellType) {
            CellType.NewsCellFeatured -> NewsFeedListFeaturedItemView(parent.context) //TODO: MAKE THIS A FEATURED CELL
            CellType.NewsCellRegular -> NewsFeedListItemView(parent.context)
        }

        return JVViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: JVViewHolder, section: Int, sectionPosition: Int, adapterPosition: Int) {
        val cellType = CellType.values()[section]

        val data: NewsFeedItem = items[adapterPosition]

        when(cellType) {
            CellType.NewsCellFeatured -> {
                viewHolder.getView<NewsFeedListFeaturedItemView>()
                    ?.apply {
                    bind(data)
                }
            }

            CellType.NewsCellRegular -> {
                viewHolder.getView<NewsFeedListItemView>()
                    ?.apply {
                        bind(data)
                    }
            }
        }
    }

    override fun getItemViewType(section: Int, position: Int, adapterPosition: Int): Int {
        return section
    }

    override fun getItemId(section: Int, sectionPosition: Int, adapterPosition: Int): Long {
        return adapterPosition.toLong()
    }
}
package com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist.components

import android.content.Context
import android.text.Html
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.ui.components.FeatureNewsImageView
import com.joshvdh.abcnewscodechallenge.ui.components.JVTextView
import com.joshvdh.abcnewscodechallenge.utils.*

/**
 *  Created by Josh van den Heever on 6/12/18.
 */
class NewsFeedListFeaturedItemView: RelativeLayout {

    var background: RelativeLayout? = null
    var image: ImageView? = null
    var titleText: JVTextView? = null
    var dateText: JVTextView? = null

    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1)
            : super(context, attrs, defStyleAttr) {

        background = RelativeLayout(context).apply {
            setBackgroundColor(JVColors.BACKGROUND_LIGHT.value)
        }
        addView(background, LayoutBuilder
            .rel(context)
            .matchWrap()
            .margin(20)
            .build())

        image = FeatureNewsImageView(context)
        background?.addView(image, LayoutBuilder
            .rel(context)
            .matchWrap()
            .build())

        titleText = JVTextView(context).apply {
            textSizeDP = 26f
            gravity = CENTER_HORIZONTAL
            setTextColor(JVColors.NEWS_HEADER.value)
        }
        background?.addView(titleText, LayoutBuilder
            .rel(context)
            .matchWrap()
            .below(image!!)
            .margin(20f)
            .build())

        //Add line
        val line = View(context).apply {
            setBackgroundColor(JVColors.NEWS_DATE.value)
        }
        background?.addView(line, LayoutBuilder.rel(context)
            .of(LayoutBuilder.MATCH, 1f)
            .below(titleText!!)
            .marginX(20f)
            .marginTop(10f)
            .build())

        dateText = JVTextView(context).apply {
            textSizeDP = 12f
            setTextColor(JVColors.NEWS_DATE.value)
        }
        background?.addView(dateText, LayoutBuilder.rel(context)
            .wrap()
            .below(line)
            .marginX(20f)
            .marginTop(5f)
            .marginBottom(20f)
            .build())
    }

    val dateFormatter = JVDateFormat.of("MMM d, yyyy")
    val timeFormatter = JVDateFormat.of("hh:mm a")
    fun bind(data: NewsFeedItem?) {
        data?.let {
            titleText?.text = Html.fromHtml(it.title)

            data.imageURL?.let { url ->
                image?.loadImageAsync(url)
            }

            //TODO: Set spannable to bolden date before time
            val dateString = dateFormatter.format(data.pubdate)
            val timeString = timeFormatter.format(data.pubdate)
            val final = String.format("<strong>%s</strong> %s", dateString, timeString)
            dateText?.text = Html.fromHtml(final)
        }
    }
}
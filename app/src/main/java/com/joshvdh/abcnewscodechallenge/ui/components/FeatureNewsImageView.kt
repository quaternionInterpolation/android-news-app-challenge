package com.joshvdh.abcnewscodechallenge.ui.components

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.widget.ImageView

/**
 *  Created by Josh van den Heever on 6/12/18.
 */
class FeatureNewsImageView: ImageView {

    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1)
            : super(context, attrs, defStyleAttr) {
        //Default
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)

        bm?.let {
            val heightMultiplier: Float = if (bm.height != 0) bm.height.toFloat() / bm.width.toFloat() else 0f
            layoutParams.height = (width.toFloat() * heightMultiplier).toInt()
            requestLayout()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }
}
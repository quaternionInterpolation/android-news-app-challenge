package com.joshvdh.abcnewscodechallenge.ui.components

import android.content.Context
import androidx.annotation.StringRes
import android.util.AttributeSet
import android.view.Gravity
import android.widget.RelativeLayout
import com.joshvdh.abcnewscodechallenge.R
import com.joshvdh.abcnewscodechallenge.utils.JVColors
import com.joshvdh.abcnewscodechallenge.utils.LayoutBuilder

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class MenuBar : RelativeLayout {
    private var leftIcon: JVTextView
    private var rightIcon: JVTextView
    private var titleView: JVTextView

    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1) : super(context, attrs, defStyleAttr) {
        //Init view
        setBackgroundColor(JVColors.BACKGROUND_LIGHT.value)

        titleView = JVTextView(context)
        titleView.textSizeDP = 26f
        titleView.setTextColor(JVColors.NEWS_HEADER.value)
        titleView.setText(R.string.menubar_default_title)
        titleView.gravity = Gravity.CENTER
        addView(titleView, LayoutBuilder.rel(context).of(LayoutParams.MATCH_PARENT, R.dimen.menubar_button_size).build())

        leftIcon = JVTextView(context)
        leftIcon.fontAwesome()
        leftIcon.textSize(R.dimen.menubar_icon_size)
        leftIcon.setTextColor(JVColors.NEWS_HEADER.value)
        leftIcon.gravity = Gravity.CENTER
        addView(leftIcon, LayoutBuilder.rel(context).of(R.dimen.menubar_button_size).build())

        rightIcon = JVTextView(context)
        rightIcon.fontAwesome()
        rightIcon.textSize(R.dimen.menubar_icon_size)
        rightIcon.setTextColor(JVColors.NEWS_HEADER.value)
        rightIcon.gravity = Gravity.CENTER
        addView(rightIcon, LayoutBuilder.rel(context).of(R.dimen.menubar_button_size).alignEnd().build())
    }

    fun title(@StringRes title: Int) = title(context.getString(title))

    fun title(title: String?): MenuBar {
        titleView.text = title
        return this
    }

    fun leftIcon(@StringRes icon: Int, callback: OnClickListener?): MenuBar {
        return leftIcon(context.getString(icon), callback)
    }

    fun leftIcon(icon: String, callback: OnClickListener?): MenuBar {
        leftIcon.text = icon
        return this
    }

    fun rightIcon(@StringRes icon: Int, callback: OnClickListener?): MenuBar {
        return rightIcon(context.getString(icon), callback)
    }

    fun rightIcon(icon: String, callback: OnClickListener?): MenuBar {
        rightIcon.text = icon
        rightIcon.setOnClickListener(callback)
        return this
    }
}
package com.joshvdh.abcnewscodechallenge.ui.newsfeed.newsfeedlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import androidx.appcompat.widget.ViewUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.joshvdh.abcnewscodechallenge.R
import com.joshvdh.abcnewscodechallenge.data.models.NewsFeedItem
import com.joshvdh.abcnewscodechallenge.ui.BaseFragment
import com.joshvdh.abcnewscodechallenge.ui.adapters.NewsFeedListAdapter
import com.joshvdh.abcnewscodechallenge.utils.ViewHelper
import com.joshvdh.abcnewscodechallenge.utils.bindView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class NewsFeedListFragment: BaseFragment<NewsFeedListPresenter>(), NewsFeedListMvpView,
    SwipeRefreshLayout.OnRefreshListener {

    override val presenter by lazy { NewsFeedListPresenter(this) }
    val adapter by lazy { NewsFeedListAdapter() }

    val swipeToRefresh by bindView<SwipeRefreshLayout>(R.id.newsfeed_list_swipe_to_refresh)
    val recyclerView by bindView<RecyclerView>(R.id.newsfeed_list_recyclerview)

    val layoutManager by lazy {
        LinearLayoutManager(
            activity,
            RecyclerView.VERTICAL,
            false
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_newsfeedlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeToRefresh.setOnRefreshListener(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    override fun onDataUpdated(items: List<NewsFeedItem>) {
        adapter.setData(items)
    }

    override fun onLoadingChanged(isLoading: Boolean) {
        swipeToRefresh.isRefreshing = isLoading
    }

    /**
     * Pull to refresh callback
     */
    override fun onRefresh() {
        presenter.refreshData()
    }
}
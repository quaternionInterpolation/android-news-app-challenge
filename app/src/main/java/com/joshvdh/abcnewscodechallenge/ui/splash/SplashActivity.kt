package com.joshvdh.abcnewscodechallenge.ui.splash

import android.os.Bundle
import com.joshvdh.abcnewscodechallenge.R
import com.joshvdh.abcnewscodechallenge.ui.BaseActivity
import com.joshvdh.abcnewscodechallenge.ui.newsfeed.NewsFeedActivity

class SplashActivity : BaseActivity<SplashPresenter>(), SplashMvpView {

    override val presenter by lazy { SplashPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        presenter.initialize()
    }

    override fun showNewsFeed() {
        presentActivity(NewsFeedActivity::class.java)
        finish()
    }
}

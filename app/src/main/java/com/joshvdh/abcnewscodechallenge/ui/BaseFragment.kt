package com.joshvdh.abcnewscodechallenge.ui

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.joshvdh.abcnewscodechallenge.mvp.MvpPresenter
import com.joshvdh.abcnewscodechallenge.mvp.MvpView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
abstract class BaseFragment<T : MvpPresenter> : MvpView, Fragment() {
    protected abstract val presenter: T?

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    //Note: This is recognisably not good, hack to keep MVP architecture in place.
    // Ideally if more time permitted, the app would use MVVM
    override fun getLifecycleOwner(): LifecycleOwner = this
}
package com.joshvdh.abcnewscodechallenge.ui.components

import android.content.Context
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.TextView
import com.joshvdh.abcnewscodechallenge.utils.DimenHelper
import com.joshvdh.abcnewscodechallenge.utils.JVFonts

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class JVTextView : TextView {
    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1)
            : super(context, attrs, defStyleAttr) {
        //Default
    }

    var textSizeDP : Float
        get() = DimenHelper.dp(textSize.toInt())
        set(value) = setTextSize(TypedValue.COMPLEX_UNIT_DIP, value)

    fun textSize(@DimenRes res: Int) {
        textSize = context.resources.getDimension(res)
    }

    fun fontAwesome() : JVTextView {
        setTypeface(JVFonts.fontAwesome)
        return this
    }

    fun fontAwesome(@StringRes symbol: Int) : JVTextView {
        fontAwesome()
        setText(symbol)
        return this
    }
}
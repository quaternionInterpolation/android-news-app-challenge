package com.joshvdh.abcnewscodechallenge.ui.newsfeed

import com.joshvdh.abcnewscodechallenge.mvp.MvpPresenter
import com.joshvdh.abcnewscodechallenge.mvp.MvpView

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
interface NewsFeedMvpView: MvpView {
    fun showNewsFeedList()
}

interface NewsFeedMvpPresenter: MvpPresenter {
    //
}
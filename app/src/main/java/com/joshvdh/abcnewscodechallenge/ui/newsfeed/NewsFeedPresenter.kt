package com.joshvdh.abcnewscodechallenge.ui.newsfeed

import com.joshvdh.abcnewscodechallenge.mvp.BasePresenter

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class NewsFeedPresenter(view: NewsFeedMvpView): BasePresenter<NewsFeedMvpView>(view), NewsFeedMvpPresenter {
    override fun initialize() {
        super.initialize()

        view.showNewsFeedList()
    }
}
package com.joshvdh.abcnewscodechallenge.utils

import org.json.JSONObject
import java.text.DateFormat
import java.text.ParseException
import java.util.*

/**
 *  Created by Josh van den Heever on 5/12/18.
 */


fun JSONObject.optDate(dateKey: String, fallback: Date?, formatter: DateFormat): Date? {
    val dateString = optString(dateKey)
    return dateString?.let {
        try {
            formatter.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }
}
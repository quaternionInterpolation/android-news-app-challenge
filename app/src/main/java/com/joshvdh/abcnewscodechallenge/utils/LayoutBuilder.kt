package com.joshvdh.abcnewscodechallenge.utils

import android.content.Context
import androidx.annotation.IdRes
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
object LayoutBuilder {
    val WRAP = ViewGroup.LayoutParams.WRAP_CONTENT
    val MATCH = ViewGroup.LayoutParams.MATCH_PARENT

    class RelativeLayoutBuilder(private var context: Context?) {
        var paramData : RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(0,0)

        constructor(context: Context, extractFrom: View) : this(context) {
            paramData = extractFrom.layoutParams as RelativeLayout.LayoutParams
        }

        fun of(dpOrRes: Number) : RelativeLayoutBuilder {
            return of(dpOrRes, dpOrRes)
        }

        fun of(widthDpOrRes: Number, heightDpOrRes : Number) : RelativeLayoutBuilder {
            paramData.apply {
                width = DimenHelper.getPxOrDimenVal(widthDpOrRes, context)
                height = DimenHelper.getPxOrDimenVal(heightDpOrRes, context)
            }
            return this
        }

        fun wrap()= of(WRAP)
        fun match()= of(MATCH)
        fun matchWrap() = of(MATCH, WRAP)
        fun wrapMatch() = of(WRAP, MATCH)

        private fun optMargin(dpOrRes: Number?, fallback: Int) : Int {
            return if(dpOrRes != null) DimenHelper.getPxOrDimenVal(dpOrRes, context) else fallback
        }


        fun margin(dpOrResStart: Number?, dpOrResEnd: Number?, dpOrResTop: Number?, dpOrResBottom: Number?) : RelativeLayoutBuilder {
            paramData.marginStart = optMargin(dpOrResStart, paramData.marginStart)
            paramData.marginEnd = optMargin(dpOrResEnd, paramData.marginEnd)
            paramData.topMargin = optMargin(dpOrResTop, paramData.topMargin)
            paramData.bottomMargin = optMargin(dpOrResBottom, paramData.bottomMargin)
            return this
        }

        fun marginStart(dpOrRes: Number?) = margin(dpOrRes, null, null, null)
        fun marginEnd(dpOrRes: Number?) = margin(null, dpOrRes, null, null)
        fun marginTop(dpOrRes: Number?) = margin(null, null, dpOrRes, null)
        fun marginBottom(dpOrRes: Number?) = margin(null, null, null, dpOrRes)

        fun marginX(dpOrRes: Number) = margin(dpOrRes, dpOrRes, null, null)
        fun marginY(dpOrRes: Number) = margin(null, null, dpOrRes, dpOrRes)
        fun margin(dpOrRes: Number) = margin(dpOrRes, dpOrRes, dpOrRes, dpOrRes)

        private fun addRule(rule: Int) : RelativeLayoutBuilder {
            paramData.addRule(rule)
            return this
        }

        private fun addRule(rule: Int, param: Int) : RelativeLayoutBuilder {
            paramData.addRule(rule, param)
            return this
        }

        fun alignStart() = addRule(RelativeLayout.ALIGN_PARENT_START)
        fun alignEnd() = addRule(RelativeLayout.ALIGN_PARENT_END)
        fun alignBottom() = addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        fun center() = addRule(RelativeLayout.CENTER_IN_PARENT)
        fun centerHorizontal() = addRule(RelativeLayout.CENTER_HORIZONTAL)
        fun centerVertical() = addRule(RelativeLayout.CENTER_VERTICAL)

        fun endOf(@IdRes viewId: Int) = addRule(RelativeLayout.END_OF, viewId)
        fun endOf(view: View) = endOf(view.generateNewViewId())

        fun startOf(@IdRes viewId: Int) = addRule(RelativeLayout.START_OF, viewId)
        fun startOf(view: View) = startOf(view.generateNewViewId())

        fun below(@IdRes viewId: Int) = addRule(RelativeLayout.BELOW, viewId)
        fun below(view: View) = below(view.generateNewViewId())

        fun above(@IdRes viewId: Int) = addRule(RelativeLayout.ABOVE, viewId)
        fun above(view: View) = above(view.generateNewViewId())

        fun build() : RelativeLayout.LayoutParams {
            return paramData
        }

        fun applyTo(view: View) {
            view.layoutParams = build()
        }
    }

    fun rel(context: Context?) : RelativeLayoutBuilder {
        return RelativeLayoutBuilder(context)
    }

    fun View.extractRelLayoutBuilder() : RelativeLayoutBuilder {
        return RelativeLayoutBuilder(context, this)
    }
}
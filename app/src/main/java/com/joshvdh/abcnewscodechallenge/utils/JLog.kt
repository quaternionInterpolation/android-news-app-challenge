package com.joshvdh.abcnewscodechallenge.utils

import android.util.Log

/**
 *  Created by Josh van den Heever on 6/12/18.
 *  NOTE: Should use Timber as clearly noted by intellisense warnings
 */
object JLog {
    //Note: Ideally would use application name, or some UUID to tag logs
    private val appTag = "JLOG"

    fun d(message: Any) = Log.d(appTag, message.toString())
    fun e(message: Any) = Log.e(appTag, message.toString())
    fun i(message: Any) = Log.e(appTag, message.toString())
}
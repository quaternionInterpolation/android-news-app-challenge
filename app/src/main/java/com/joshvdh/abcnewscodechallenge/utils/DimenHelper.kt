package com.joshvdh.abcnewscodechallenge.utils

import android.content.Context
import android.util.Log
import android.view.ViewGroup
import com.joshvdh.abcnewscodechallenge.R

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
object DimenHelper {
    private const val INIT_ERROR_MSG: String = "Cannot call functions in DimenHelper.kt, must call init first!"

    const val MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT;
    const val WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT;
    private var density: Float = 0f

    fun px(dp: Float) : Int {
        if (density == 0f) throw Exception(INIT_ERROR_MSG)
        return (dp * density).toInt()
    }

    fun dp(px: Int) : Float {
        if (density == 0f) throw Exception(INIT_ERROR_MSG)
        return (px.toFloat() / density)
    }

    fun getPxOrDimenVal(dpOrDimenRes: Number, context: Context?) : Int {
        return if (dpOrDimenRes == MATCH_PARENT || dpOrDimenRes == WRAP_CONTENT) {
            dpOrDimenRes as Int
        } else if (isId(dpOrDimenRes)) {
            context?.resources?.getDimensionPixelSize(dpOrDimenRes as Int) ?: {
                Log.e("DimenHelper", "Cannot retrieve pixel size for ID $dpOrDimenRes")
                0
            }()
        } else {
            px(dpOrDimenRes.toFloat())
        }
    }

    fun isId(valueOrIdRes: Number) : Boolean {
        return valueOrIdRes.toInt() > R.dimen.aaaaaaaa_first
    }

    fun init(context: Context) {
        density = context.resources.displayMetrics.density
    }
}
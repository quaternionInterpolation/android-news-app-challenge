package com.joshvdh.abcnewscodechallenge.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
class MainThreadExecutor: Executor {
    private val handler: Handler by lazy {
         Handler(Looper.getMainLooper())
    }

    override fun execute(command: Runnable) {
        handler.post(command)
    }

    fun executeDelayed(delayTime: Long, command: ()->Unit) {
        handler.postDelayed(command, delayTime)
    }

    fun stop(command: ()->Unit) {
        handler.removeCallbacks(command )
    }
}
package com.joshvdh.abcnewscodechallenge.utils

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import com.joshvdh.abcnewscodechallenge.R

/**
 *  Created by Josh van den Heever on 4/12/18.
 */

enum class JVColors(
    @field:ColorRes val colorRes: Int
) {
    BACKGROUND_DARK(R.color.news_background_dark),
    BACKGROUND_LIGHT(R.color.news_background_light),
    NEWS_HEADER(R.color.news_header),
    NEWS_DATE(R.color.news_date);

    @ColorInt var value: Int = 0

    companion object {
        fun init(context: Context) {
            val resources = context.resources
            values().forEach {
                it.value = resources.getColor(it.colorRes)
            }
        }
    }
}
package com.joshvdh.abcnewscodechallenge.utils

import android.content.Context
import android.graphics.Typeface

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
object JVFonts {
    private val FONT_PREFIX : String = "fonts/"

    lateinit var fontAwesome : Typeface

    fun init(context: Context) {
        fontAwesome = loadFont(context, "FontAwesome.otf")
    }

    private fun loadFont(context: Context, fontName: String) : Typeface {
        return Typeface.createFromAsset(context.assets, FONT_PREFIX + fontName)
    }
}
package com.joshvdh.abcnewscodechallenge.utils

import android.view.View
import androidx.annotation.ColorRes

/**
 *  Created by Josh van den Heever on 6/12/18.
 */
inline fun View.setBackgroundColorRes(@ColorRes colorRes: Int) {
    setBackgroundColor(resources.getColor(colorRes))
}
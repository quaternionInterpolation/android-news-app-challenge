package com.joshvdh.abcnewscodechallenge.utils

import android.widget.ImageView
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import java.io.IOException
import java.net.URL


/**
 *  Created by Josh van den Heever on 6/12/18.
 */
object AsyncImageLoader {
    private val activeLoadMap = mutableMapOf<ImageView, LoadImageRunnable>()

    fun loadImageInto(url: String, imageView: ImageView) {
        val taskRunner = LoadImageRunnable(url, imageView)
        activeLoadMap[imageView] = taskRunner
        TaskManager.postToWorker(taskRunner)
    }

    class LoadImageRunnable(urlString: String, private val imageView: ImageView): Runnable {
        private val url = URL(urlString )
        var cancelled: Boolean = false

        override fun run() {
            try {
                val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                if (!cancelled) {
                    applyImageToView(bmp)
                } else {
                    bmp.recycle()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                //Remove self from active load map
                activeLoadMap.remove(imageView)
            }
        }

        fun applyImageToView(bmp: Bitmap) {
            TaskManager.post {
                imageView.setImageBitmap(bmp)
            }
        }
    }
}

//Cheeky wee extension method
fun ImageView.loadImageAsync(url: String) {
    AsyncImageLoader.loadImageInto(url, this)
}
package com.joshvdh.abcnewscodechallenge.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
class JVDateFormat(pattern: String): SimpleDateFormat(pattern, Locale.getDefault()) {
    companion object {
        //Could end up using these everywhere, worth caching them
        val dateFormats = mutableMapOf<String, JVDateFormat>()

        fun of(pattern: String): JVDateFormat {
            return dateFormats[pattern] ?: let {
                val format = JVDateFormat(pattern)
                dateFormats[pattern] = format
                format
            }
        }
    }
}
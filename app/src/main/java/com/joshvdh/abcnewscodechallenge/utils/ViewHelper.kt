package com.joshvdh.abcnewscodechallenge.utils

import android.os.Build
import android.view.View
import java.util.concurrent.atomic.AtomicInteger

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
object ViewHelper {
    private var idGenerator: AtomicInteger = AtomicInteger(1)

    fun generateViewId(): Int {
        while (true) {
            val result = idGenerator.get()
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            var newValue = result + 1
            if (newValue > 0x00FFFFFF) newValue = 1 // Roll over to 1, not 0.
            if (idGenerator.compareAndSet(result, newValue)) {
                return result
            }
        }
    }
}

fun View.generateNewViewId() : Int {
    id = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
        View.generateViewId()
    } else {
        ViewHelper.generateViewId()
    }
    return id
}
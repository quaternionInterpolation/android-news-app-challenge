package com.joshvdh.abcnewscodechallenge.utils

import androidx.annotation.NonNull
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
object TaskManager {
    private val mainThreadExecutor: MainThreadExecutor by lazy { MainThreadExecutor() }
    private val workThreadPoolExecutor: Executor = Executors.newCachedThreadPool()

    fun post(command: ()->Unit) {
        mainThreadExecutor.execute(command)
    }

    fun postDelayed(delayTime: Long, command: ()->Unit) {
        mainThreadExecutor.executeDelayed(delayTime, command)
    }

    fun cancelPost(command: ()->Unit) {
        mainThreadExecutor.stop(command)
    }

    fun postToWorker(@NonNull command: ()->Unit) {
        workThreadPoolExecutor.execute(command)
    }

    fun postToWorker(@NonNull command: Runnable) {
        workThreadPoolExecutor.execute(command)
    }
}
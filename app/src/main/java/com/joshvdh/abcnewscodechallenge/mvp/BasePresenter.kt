package com.joshvdh.abcnewscodechallenge.mvp

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
abstract class BasePresenter<T : MvpView>(protected var view: T) {
}
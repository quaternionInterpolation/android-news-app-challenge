package com.joshvdh.abcnewscodechallenge.mvp

import androidx.lifecycle.LifecycleOwner

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
interface MvpView {
    fun getLifecycleOwner(): LifecycleOwner
}
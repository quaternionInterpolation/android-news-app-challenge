package com.joshvdh.abcnewscodechallenge.mvp

/**
 *  Created by Josh van den Heever on 4/12/18.
 */
interface MvpPresenter {
    fun initialize(){}
    fun onResume(){}
    fun onPause(){}
    fun onDestroy(){}
}
package com.joshvdh.abcnewscodechallenge.network

import org.json.JSONObject

/**
 *  Created by Josh van den Heever on 5/12/18.
 */
interface IJsonSerializable {
    fun fromJson(json: JSONObject): IJsonSerializable
    fun toJson(): JSONObject? = null
}
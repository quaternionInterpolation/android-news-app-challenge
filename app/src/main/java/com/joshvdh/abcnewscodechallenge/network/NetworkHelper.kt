package com.joshvdh.abcnewscodechallenge.network

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.joshvdh.abcnewscodechallenge.data.DatabaseHelper
import com.joshvdh.abcnewscodechallenge.data.temp.NewsFeedData

/**
 *  Created by Josh van den Heever on 5/12/18.
 */

object NetworkHelper {
    private val ENDPOINT_FEED = "https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml"

    //Should move custom status codes into their own structure
    private val STATUSCODE_NEWSFEED_ISSUE = 509

    private lateinit var requestQueue: RequestQueue

    interface NetworkCallback {
        fun onResult(success: Boolean, code: Int? = null, message: String? = null)
    }

    fun fetchFeed(callback: NetworkCallback?) {
        JsonObjectRequest(
            Request.Method.GET,
            ENDPOINT_FEED,
            null,
            Response.Listener {
                //Parse list of items
                val newsFeedData = NewsFeedData()
                    .fromJson(it)

                if (newsFeedData.status != NewsFeedData.Status.OK) {
                    //There was an issue in the API
                    callback?.onResult(false, STATUSCODE_NEWSFEED_ISSUE, null)
                    return@Listener
                }

                //Add news items into the database
                DatabaseHelper.addNewsFeedItems(*newsFeedData.parsedItems.toTypedArray())

                callback?.onResult(true)
            },
            Response.ErrorListener {
                callback?.onResult(false, it.networkResponse.statusCode, it.message)
            }
        ).also {
            requestQueue.add(it)
        }
    }

    fun init(appContext: Context) {
        requestQueue = Volley.newRequestQueue(appContext)
    }

    fun cancelAll() {
        requestQueue?.cancelAll(NetworkHelper)
    }
}